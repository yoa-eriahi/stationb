<footer class="py-5">
    <div class="d-flex align-items-center flex-column content text-center">
        <span>conact@coworking.com</span>
        <span>+216 52 170 769</span>
        <span>2 rue Fer du Moulin, 75005 Paris</span>
        <span>48 bis rue d'Alésia, 75014 Paris</span>
        <div class="social-networks my-3 d-flex">
            <h2 class="mx-3"><i class="fab fa-facebook-f"></i></h2>
            <h2 class="mx-3"><i class="fab fa-twitter"></i></h2>
            <h2 class="mx-3"><i class="fab fa-instagram"></i></h2>
        </div>
        <span>Conditions Générales d'utilisations | FAQ | Blog</span>
        <hr>
        <span class="text-muted">2019 &copy; Coworking</span>
    </div>
</footer>
