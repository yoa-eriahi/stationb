<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script href="{{ asset('js/app.js') }}" defer></script>
    <title>Station B</title>
</head>
<body>
<div id="app">
    <header>
        @include('partials._nav')

    </header>
    <article class="py-5">
        <section class="custom-carousel">
            <div class="d-flex align-items-center">
                <div class="img-carousel-wrapper col-md-3 p-1 p-md-0">
                    <img src="https://picsum.photos/400/300/?random" alt="" class="w-100 overflow-hidden">
                </div>
                <div class="img-carousel-wrapper col-md-3 p-1 p-md-0">
                    <img src="https://picsum.photos/400/300/?random" alt="" class="w-100 overflow-hidden">
                </div>
                <div class="img-carousel-wrapper col-md-3 p-1 p-md-0">
                    <img src="https://picsum.photos/400/300/?random" alt="" class="w-100 overflow-hidden">
                </div>
                <div class="img-carousel-wrapper col-md-3 p-1 p-md-0">
                    <img src="https://picsum.photos/400/300/?random" alt="" class="w-100 overflow-hidden">
                </div>
            </div>
        </section>
        <section id="coworking" class="bg-black text-white py-5 welcome-wrapper">
            <div class="container">
                <h1 class="mb-0 welcome">Bienvenue</h1>
                <hr class="mt-1 co-divider mx-0 mb-3">
                <p class="coworking-desc">Station B, ce sont des espaces de travail au coeur de Sfax. Ouverts 24h/24 et
                    7j/7,
                    accessible à tous ceux qui veulent travailler sereinement sans faire pleurer leur
                    portefeuille</p>
            </div>
        </section>
        <section class="py-5">
            <div class="container">
                <h1 class="font-weight-bold text-black">Station B, c'est pour qui ?</h1>
                <hr class="co-divider bg-black mx-0 mt-1 mb-3">
                <div class="row">
                    <div class="col-md-6 my-5">
                        <h2 class="font-weight-bold">Les étudiants...</h2>
                        <h4>...qui en ont marre de faire la queue devant les bibliothèques</h4>
                    </div>
                    <div class="col-md-6 my-5">
                        <h2 class="font-weight-bold">Les doctorants, les profs et les chercheurs...</h2>
                        <h4>...qui ont besoin d'un environnement de travail stimulant</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 my-5">
                        <h2 class="font-weight-bold">Les startupers, les freelances, les auto-entrepreneurs...</h2>
                        <h4>...qui cherchent un endroit pour travailler sans se ruiner</h4>
                    </div>
                    <div class="col-md-6 my-5">
                        <h2 class="font-weight-bold">Et aussi tous les autres</h2>
                        <h4>...les vieux, les moins vieux, les rêveurs, les bosseurs, les fainéants, les fous de
                            lecture, les sans connnexion internet fixe</h4>
                    </div>
                </div>
            </div>
        </section>
        <section class="my-5">
            <div class="container text-center"><h1>En bref, la performance <br>c'est pour tout le <br> monde</h1></div>
        </section>
        <section id="accueil" class="bg-white d-flex flex-column align-items-center justify-content-center">
            <div class="text-center register-form">
                <h1 class="text-black my-5">Souscris à notre <span class="font-weight-bold">NewsLetter</span> pour avoir les dernières informations :)</h1>
                <div class="container my-2">
                    <form action="">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control text-center border-0 shadow-sm co-custom-control"
                                   placeholder="saisis ton adresse mail">
                        </div>
                        <button class="btn btn-block btn-outline-black shadow-sm" type="submit">Recevoir des mails</button>
                    </form>
                </div>
            </div>
            <div class="adverts my-4 text-center w-100 d-flex align-items-center flex-column">
                <p class="small text-black">Places disponibles actuellement</p>
                <div class="advert-details row small font-weight-bold w-100 justify-content-center">
                    <div class="col-md-6 advert px-0 mx-2 mt-1">
                        <p class="description mb-1">2 rue du Fer à Moulin 75005 Paris</p>
                        <div class="places-number py-1 bg-black text-white">
                            <h1 class="number font-weight-bold">56</h1>Places
                        </div>
                    </div>
                    <div class="col-md-6 advert px-0 mx-2 mt-1">
                        <p class="description mb-1">48 bis rue d'Alésia 75014 Paris</p>
                        <div class="places-number py-1 bg-black text-white">
                            <h1 class="number font-weight-bold">72</h1>Places
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="parallax"></section>
        <section class="container-modal text-center d-flex justify-content-center">
            <div class="card pt-4 border-0 shadow">
                <div class="card-body flex-column d-flex align-items-center justify-content-center">
                    <h3 class="mb-5">Inscris-toi à <strong>Station B</strong> et profite dès maintenant de nos espaces
                        de travail</h3>
                    <button class="btn bg-black btn-lg text-white">S'inscrire maintenant</button>
                </div>
            </div>
        </section>

        <section class="pb-5" id="services">
            <div class="container">
                <h1 class="font-weight-bold text-black">Les services</h1>
                <hr class="co-divider bg-black mx-0 mt-1 mb-3">
                <div class="row">
                    <div class="col-md-4 my-2">
                        <div class="card service border-0 shadow-sm">
                            <div class="card-body d-flex flex-column justify-content-center text-center">
                                <h1 class="display-4"><i class="fas fa-wifi mb-3"></i></h1>
                                <h2 class="font-weight-bold mb-4">Le wifi</h2>
                                <h5>...le plus rapide de la ville. La fibre optique à son plus haut niveau et totalement
                                    gratuite bien sûr.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 my-2">
                        <div class="card service border-0 shadow-sm">
                            <div class="card-body d-flex flex-column justify-content-center text-center">
                                <h1 class="display-4"><i class="fas fa-lock"></i></h1>
                                <h2 class="font-weight-bold mb-4">Les casiers</h2>
                                <h5>...les plus pratiques de la région. Epargne ton dos, tu peux laisser tes affaires
                                    sur place.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 my-2">
                        <div class="card service border-0 shadow-sm">
                            <div class="card-body d-flex flex-column justify-content-center text-center">
                                <h1 class="display-4"><i class="fas fa-print mb-3"></i></h1>
                                <h2 class="font-weight-bold mb-4">Les impressions</h2>
                                <h5>...les moins chères du pays. Des petits prix pour l'impression, la copie et le
                                    scan.</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-md-3 mt-0">
                    <div class="col-md-4 my-2">
                        <div class="card service border-0 shadow-sm">
                            <div class="card-body d-flex flex-column justify-content-center text-center">
                                <h1 class="display-4"><i class="fas fa-users mb-3"></i></h1>
                                <h2 class="font-weight-bold mb-4">Les salles de réunions</h2>
                                <h5>...les plus stylées du continent.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 my-2">
                        <div class="card service border-0 shadow-sm">
                            <div class="card-body d-flex flex-column justify-content-center text-center">
                                <h1 class="display-4"><i class="fas fa-couch mb-3"></i></h1>
                                <h2 class="font-weight-bold mb-4">Les espaces détente</h2>
                                <h5>...les plus chill du monde. Café, snacks, canapés et fauteuils t'offriront une pause
                                    tout en douceur.</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 my-2">
                        <div class="card service border-0 shadow-sm">
                            <div class="card-body d-flex flex-column justify-content-center text-center">
                                <h1 class="display-4"><i class="fas fa-home mb-3"></i></h1>
                                <h2 class="font-weight-bold mb-4">la domiciliation d'entreprise</h2>
                                <h5>...la plus réactive de l'univers. Tu crées ta boîte et tu n'as pas de siège social ?
                                    Nous pouvons domicilier ton entreprise et recevoir ton courrier.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="py-5 bg-white" id="tarifs">
            <div class="container">
                <h1 class="font-weight-bold text-black">Les tarifs</h1>
                <hr class="co-divider bg-black mx-0 mt-1 mb-3">
            </div>
            <div class="prices">
                <div class="d-flex flex-wrap justify-content-center px-5">
                    <div class="mx-5 my-4 price">
                        <h3 class="text-uppercase">à l'heure</h3>
                        <div class="price-card">
                            <div class="card border-0 bg-black text-white rounded-0">
                                <div
                                    class="card-body d-flex flex-column align-items-center justify-content-between py-5">
                                    <img src="{{asset('assets/h-creuse.png')}}" alt="">
                                    <p><strong>1€/H </strong>EN HEURE CREUSE</p>
                                    <img src="{{asset('assets/h-pleine.png')}}" alt="">
                                    <p><strong>1,5€/H </strong>EN HEURE PLEINE</p><span class="separator"></span>
                                    <p>24h/24</p><span class="separator"></span>
                                    <p>7j/7</p><span class="desc">L'inscription est gratuite. Les sommes dues sont prélevées après chaque passage.</span>
                                    <a class="btn btn-block bg-white text-black mt-3">S'inscrire</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mx-5 my-4 price">
                        <h3 class="text-uppercase">abonnement limité</h3>
                        <div class="price-card">
                            <div class="card border-0 bg-black text-white rounded-0">
                                <div
                                    class="card-body d-flex flex-column align-items-center justify-content-between py-5">
                                    <p>A partir de <strong style="display: inline"> 99€ / MOIS</strong></p>
                                    <span
                                        class="separator"></span>
                                    <strong>ACCÉS SANS LIMITE</strong>
                                    <span
                                        class="separator"></span>
                                    <p class="m-0">24h/24</p>
                                    <hr class="divider bg-white">
                                    <p class="m-0">7j/7</p>
                                    <hr class="divider bg-white">
                                    <strong>SANS ENGAGEMENT</strong>
                                    <span
                                        class="desc">Renouvellement mensuel automatique</span>
                                    <a class="btn btn-block btn-outline-light text-white mt-3">S'abonner</a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mx-5 my-4 price">
                        <h3 class="text-uppercase">les petits plus</h3>
                        <div class="price-card">
                            <div class="card rounded-0 bg-white border-black text-black">
                                <div
                                    class="card-body d-flex flex-column align-items-center justify-content-between py-5">
                                    <span class="alheure-vertical">Les petits plus</span>
                                    <p>Impression</p>
                                    <strong>Noir et Blanc : 0,08€</strong>
                                    <strong>Couleur :
                                        0,30€</strong>
                                    <strong>Scan : gratuit</strong>
                                    <hr class="divider bg-white">
                                    <p>SALLE DE RÉUNION </p>
                                    <strong>A partir de 5€ / heure</strong>
                                    <hr class="divider bg-white">
                                    <p>CASIERS</p>
                                    <strong>Le petit: 17€/mois</strong>
                                    <strong>Le grand: 22€/mois</strong>
                                    <span class="separator"></span>
                                    <p>DOMICILIATION</p>
                                    <strong>A partir de 29,90€/mois</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5 bg-light" id="contact">
            <div class="container">
                <h1 class="font-weight-bold text-black">Envoyez nous un message</h1>
                <hr class="co-divider bg-black mx-0 mt-1 mb-3">
                <h5 class="font-italic">Pour nous contacter</h5>
                <div class="message-form py-5 mx-auto d-flex flex-column">
                    <form action="" class="py-5">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control shadow-sm border-0 rounded-0 co-custom-control" id="name" name="name" placeholder="Nom">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control shadow-sm border-0 rounded-0 co-custom-control" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control shadow-sm border-0 rounded-0 co-custom-textarea" id="message" name="message" placeholder="Message"></textarea>
                        </div>
                        <button class="btn btn-block bg-black text-white btn-lg">Envoyer</button>
                    </form>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d104888.65493704632!2d10.663058760310939!3d34.76137444792756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13002cda1486c695%3A0x22dfe0a62c50ce6f!2sSfax!5e0!3m2!1sfr!2stn!4v1554813658325!5m2!1sfr!2stn" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
        @include('partials._footer')
    </article>
</div>
</body>
</html>
